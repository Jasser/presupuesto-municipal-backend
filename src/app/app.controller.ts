import { controller } from '@foal/core';

import { ApiController} from './controllers';
import { LoginController } from './controllers/login.controller';

export class AppController {
  subControllers = [
    controller('/api', ApiController),
    controller('/api/auth', LoginController)
  ];
}
