import { Context, Get, HttpResponseCreated, HttpResponseOK, Post } from '@foal/core';
import { JWTRequired } from '@foal/jwt';
import { getRepository } from 'typeorm';
import { Comment } from '../entities/comment.entity';
import { Post as PostTodo } from '../entities/post.entity';
import { RefreshJWT } from '../hooks/refresh-jwt.hook';

export class ApiController {

  @Get('/posts')
  async getTodos(ctx: Context) {
    var queryHash = {};

    if (ctx.request.query.departamentoId !== '' &&
        ctx.request.query.departamentoId !== 'Todos los departamentos' &&
        ctx.request.query.departamentoId !== undefined ) {
      // tslint:disable-next-line: no-string-literal
      queryHash['departamentoId'] =  ctx.request.query.departamentoId;
    }

    if (ctx.request.query.usuarioId !== '' &&
        ctx.request.query.usuarioId !== undefined ) {
        // tslint:disable-next-line: no-string-literal
        queryHash['usuarioId'] =  ctx.request.query.usuarioId;
      }

    // tslint:disable-next-line: no-trailing-whitespace
    if (ctx.request.query.municipioId !== 'Todos los municipios' &&
        ctx.request.query.departamentoId !== 'Todos los departamentos' &&
        ctx.request.query.municipioId !== '' &&
        ctx.request.query.municipioId !== undefined) {
      // tslint:disable-next-line: no-string-literal
      queryHash['municipioId'] =  ctx.request.query.municipioId;
    }

    if (ctx.request.query.sectorId !== '' &&
        ctx.request.query.sectorId !== 'Todos los sectores' &&
        ctx.request.query.sectorId !== undefined) {
      // tslint:disable-next-line: no-string-literal
      queryHash['sectorId'] =  ctx.request.query.sectorId;
    }

    const posts = await getRepository(PostTodo)
    .createQueryBuilder('post')
    .where(queryHash)
    .orderBy('post.id', 'DESC')
    .getMany();
    return new HttpResponseOK(posts);
  }

  @Post('/posts')
  async potTodo(ctx: Context) {
    const post = new PostTodo();
    post.usuarioId = ctx.request.body.usuarioId;
    post.nombreUsuario = ctx.request.body.nombreUsuario;
    post.title = ctx.request.body.title;
    post.body = ctx.request.body.body;
    post.departamentoId = ctx.request.body.departamentoId;
    post.municipioId = ctx.request.body.municipioId;
    post.sectorId = ctx.request.body.sectorId;
    post.fecha = new Date().toISOString().slice(0,10);

    await getRepository(PostTodo).save(post);
    return new HttpResponseCreated(post);
  }

  @Get('/comments')
  async getComments(ctx: Context) {
    const comments = await getRepository(Comment)
    .createQueryBuilder('comment')
    .where('comment.postId = :postId', { postId: ctx.request.query.postId})
    .orderBy("comment.id", "DESC")
    .getMany();
    return new HttpResponseOK(comments);
  }

  @Post('/comments')
  async postComment(ctx: Context) {
    const comment = new Comment();
    comment.usuarioId = ctx.request.body.usuarioId;
    comment.nombreUsuario = ctx.request.body.nombreUsuario;
    comment.body = ctx.request.body.body;
    comment.postId = ctx.request.body.postId;
    comment.fecha = new Date().toISOString().slice(0,10);
    await getRepository(Comment).save(comment);
    return new HttpResponseCreated(comment);
  }

  @Post('/graph')
  async postGraphic(ctx: Context) {
    let queryHash = {};

    queryHash['departamentoId'] =  ctx.request.body.departamento;
    queryHash['municipioId'] =  ctx.request.body.municipio;
    const list_m = ctx.request.body.list;
    const sector = ctx.request.body.sector;

    const cant_post = await getRepository(PostTodo)
    .createQueryBuilder('post')
    .where(queryHash)
    .getCount();

    const post = await getRepository(PostTodo)
    .createQueryBuilder('post')
    .select("post.id")
    .where(queryHash)
    .getMany();

    let id_posts = post.map(p => p.id.toString());
    const cant_comment = await getRepository(Comment)
    .createQueryBuilder('comment')
    .where('comment.postId IN (:...postIds)', { postIds: id_posts})
    .getCount();

    //    .select('DISTINCT ports.port_name', 'port_names')
    const cant_usuarios = await getRepository(PostTodo)
    .createQueryBuilder('post')
    .select('DISTINCT post.usuarioId ', 'uid')
    .where(queryHash)
    .getCount();

    console.log(cant_usuarios);

    const sectores = 
    ['Administración gubernamental',
    'Servicios de defensa, orden público y seguridad',
    'Servicios económicos',
    'Deuda pública, intereses y gastos',
    'Educación',
    'Salud',
    'Servicios sociales y asistencia social',
    'Vivienda y servicios comunitarios',
    'Servicios recreativos, culturales y religiosos'];

    let listMunicipios = [];

    function reject(obj : any, keys: any) {
      return Object.keys(obj)
          .filter(k => keys.includes(k))
          .map(k => Object.assign({}, {[k]: obj[k]}))
          .reduce((res, o) => Object.assign(res, o), {});
    }

    listMunicipios = await Promise.all (
              list_m.map(async m => {
                  return await {
                    'month': m,
                    'Administración gubernamental': await this.obtenerCanTSect( m, sectores[0]),
                    'Servicios de defensa, orden público y seguridad': await this.obtenerCanTSect( m, sectores[1]),
                    'Servicios económicos': await this.obtenerCanTSect( m, sectores[2]),
                    'Deuda pública, intereses y gastos': await this.obtenerCanTSect( m, sectores[3]),
                    'Educación': await this.obtenerCanTSect( m, sectores[4]),
                    'Salud': await this.obtenerCanTSect( m, sectores[5]),
                    'Servicios sociales y asistencia social': await this.obtenerCanTSect( m, sectores[6]),
                    'Vivienda y servicios comunitarios': await this.obtenerCanTSect( m, sectores[7]) ,
                    'Servicios recreativos, culturales y religiosos': await this.obtenerCanTSect( m, sectores[8])
                  };
              })
            );

    let municpio_filt = listMunicipios.filter(l => l['month'] === ctx.request.body.municipio )[0];

    let data_muncipio = [
      {
      name: 'Administración gubernamental',
      value: municpio_filt['Administración gubernamental']
      },
      {
      name: 'Servicios de defensa, orden público y seguridad',
      value: municpio_filt['Servicios de defensa, orden público y seguridad']
      },
      {
      name: 'Servicios económicos',
      value: municpio_filt['Servicios económicos']
      },
      {
      name: 'Deuda pública, intereses y gastos',
      value: municpio_filt['Deuda pública, intereses y gastos']
      },
      {
      name: 'Educación',
      value: municpio_filt['Educación']
      },
      {
      name: 'Salud',
      value: municpio_filt['Salud']
      },
      {
      name: 'Servicios sociales y asistencia social',
      value: municpio_filt['Servicios sociales y asistencia social']
      },
      {
      name: 'Vivienda y servicios comunitarios',
      value: municpio_filt['Vivienda y servicios comunitarios']
      },
      {
      name: 'Servicios recreativos, culturales y religiosos',
      value: municpio_filt['Servicios recreativos, culturales y religiosos']
      }
    ]


    const fuck = {
      list_muncipios: listMunicipios.map( a => reject(a, ['month', sector])),
      sect_municipio: data_muncipio,
      num_post : cant_post,
      num_comment: cant_comment,
      num_usuarios: cant_usuarios
    };
    return new HttpResponseCreated(fuck);
  }

  async obtenerCanTSect( municipioId, sectorId) {
          // tslint:disable-next-line: align
          return await getRepository(PostTodo)
                      .createQueryBuilder('post')
                      .andWhere('post.municipioId = :municipio', {municipio: municipioId})
                      .andWhere('post.sectorId = :sector', {sector: sectorId})
                      .getCount();
  }

}
