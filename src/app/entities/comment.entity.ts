// @ts-ignore : 'Column' is declared but its value is never read.
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Comment {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  usuarioId: string;

  @Column()
  postId: string;

  @Column()
  nombreUsuario: string;

  @Column()
  body: string;

  @Column()
  fecha: string;
}
