// @ts-ignore : 'Column' is declared but its value is never read.
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Post {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  usuarioId: string;

  @Column()
  nombreUsuario: string;

  @Column()
  municipioId: string;

  @Column()
  departamentoId: string;

  @Column()
  sectorId: string;

  @Column()
  title: string;

  @Column()
  body: string;

  @Column()
  fecha: string;
}
