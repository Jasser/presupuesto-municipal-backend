import { Config, Hook, HookDecorator, HttpResponse } from '@foal/core';
import { sign } from 'jsonwebtoken';

export function RefreshJWT(): HookDecorator {
  return Hook(ctx => {
    if (!ctx.user) {
      return;
    }

    return (ctx, services, response: HttpResponse) => {
      const newToken = sign(
        // The below object assumes that ctx.user is
        // the decoded payload (default behavior).
        {
          email: ctx.user.email,
          // id: ctx.user.id,
          // sub: ctx.user.subject,
        },
        Config.get<string>('settings.jwt.secretOrPublicKey'),
        { expiresIn: '15m' }
      );
      response.setHeader('Authorization', newToken);
    };

  });
}
