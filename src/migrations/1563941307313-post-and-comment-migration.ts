import {MigrationInterface, QueryRunner} from "typeorm";

export class postAndCommentMigration1563941307313 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "comment" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "usuarioId" varchar NOT NULL, "postId" varchar NOT NULL, "nombreUsuario" varchar NOT NULL, "body" varchar NOT NULL)`);
        await queryRunner.query(`CREATE TABLE "post" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "usuarioId" varchar NOT NULL, "nombreUsuario" varchar NOT NULL, "municipioId" varchar NOT NULL, "departamentoId" varchar NOT NULL, "sectorId" varchar NOT NULL, "title" varchar NOT NULL, "body" varchar NOT NULL)`);
        await queryRunner.query(`CREATE TABLE "user" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "email" varchar NOT NULL, "password" varchar NOT NULL, CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22" UNIQUE ("email"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DROP TABLE "user"`);
        await queryRunner.query(`DROP TABLE "post"`);
        await queryRunner.query(`DROP TABLE "comment"`);
    }

}
