// 3p
import { createConnection, getConnection, getManager } from 'typeorm';

// investment-plan
import {Post} from '.././app/entities/post.entity';

export const schema = {
  additionalProperties: true,
  properties: {
  },
  required: [ /* To complete */ ],
  type: 'object',
};

export async function main(args) {
  await createConnection();

  const post = new Post();
  post.departamentoId = args.departamentoId;
  post.usuarioId = args.usuarioId;
  post.nombreUsuario = args.nombreUsuario;
  post.sectorId = args.sectorId;
  post.municipioId = args.municipioId;
  post.title = args.title;
  post.body = args.body;

  //foal run create-investment-plan departamentoId="1" usuarioId="1" nombreUsuario="Jasser" sectorId="1" municipioId="1" title="soy el mas perron" body="nada que ver"
  try {
    console.log(
      await getManager().save(post)
    );
  } catch (error) {
    console.log(error.message);
  }

  await getConnection().close();
}
